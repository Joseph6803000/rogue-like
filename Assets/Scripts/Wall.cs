﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

	public int hitPoints = 2;
	public Sprite damagedWallSprite;
	//place 48-54 53 repeats

	private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();



	}
	
	public void DamageWall(int damagedRecieved){
		hitPoints -= damagedRecieved;
		spriteRenderer.sprite = damagedWallSprite;
		if(hitPoints <= 0){
			gameObject.SetActive(false);
		}

	}
}